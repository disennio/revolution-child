<?php get_header(); ?>
<?php
	$article_sidebar     = ot_get_option( 'article_sidebar', 'on' );
	$article_author_name = ot_get_option( 'article_author_name', 'on' );
	$article_date        = ot_get_option( 'article_date', 'on' );
	$article_cat         = ot_get_option( 'article_cat', 'on' );
?>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1" fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
</script>
<!-- facebook Script End -->

<article itemscope itemtype="http://schema.org/Article" <?php post_class( 'post post-detail style1-detail' ); ?>>
	
	<div class="row align-center">
		<div class="small-12 medium-12 large-12 columns">
			<div class="post-content">
                
                <header class="post-title entry-header animation bottom-to-top-3d victim-post-title">
					
<?php 
		if(get_field('victim_photo')){
			$feat_image_url = get_field('victim_photo');
		} else {
			if(get_field('victim_gender') == 2){
				$feat_image_url = get_template_directory_uri().'/assets/img/user-2.png';
			} else {
				$feat_image_url = get_template_directory_uri().'/assets/img/user.png';
			}
		}
		
?>                   
                    <img class="victim_photo" src="<?php echo $feat_image_url; ?>" alt="">

					<div>

                        <h2><?php echo the_field('victim_name') . ' ' . the_field('victim_lastname'); ?></h2>

                        <p>
                          <strong>Edad: <?php echo get_field('victim_age'); ?>,</strong>  
                          <strong>Género: <?php echo get_field('victim_gender'); ?></strong>  
                        </p>
                        <?php 
                            $perpetrators = get_field('victim_perpetrators');
                        ?>
                        <p>
                            <span>Fecha: <?php echo get_field('victim_date'); ?><span>, </span></span>
                            <span>Tipo Victima: <?php echo get_field('victim_type'); ?><span>, </span></span> <br>
                            <span>Departamento: <?php echo get_field('victim_location_department'); ?><span>, </span></span>
                            <span>Municipio: <?php echo get_field('victim_location_city'); ?><span>, </span></span>
                            <span>Lugar: <?php echo get_field('victim_location_place'); ?> <br>
                            <span>Presuntos responsables: <br>
                            <?php 
                                if ( $perpetrators ) {
                                    echo implode( ', ', $perpetrators );
                                }
                            ?></span>
                        </p>
                        

                    </div>
                        <p>

<a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_post_permalink(); ?>">
	<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	        <g id="Group" fill="#000000" fill-rule="nonzero">
	            <path d="M12,0 C5.373,0 0,5.373 0,12 C0,18.627 5.373,24 12,24 C18.627,24 24,18.627 24,12 C24,5.373 18.627,0 12,0 Z M15,8 L13.65,8 C13.112,8 13,8.221 13,8.778 L13,10 L15,10 L14.791,12 L13,12 L13,19 L10,19 L10,12 L8,12 L8,10 L10,10 L10,7.692 C10,5.923 10.931,5 13.029,5 L15,5 L15,8 Z" id="Shape"></path>
	        </g>
	    </g>
	</svg>
</a>

<a target="_blank" href="whatsapp://send?text=<?php echo get_post_permalink(); ?>" data-action="share/whatsapp/share">
	<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	        <g id="Group" fill="#000000" fill-rule="nonzero">
	            <path d="M12.031,6.172 C8.85,6.172 6.264,8.758 6.26299608,11.938 C6.262,13.236 6.643,14.208 7.282,15.225 L6.7,17.353 L8.882,16.78 C9.86,17.36 10.793,17.708 12.027,17.709 C15.205,17.709 17.794,15.122 17.7950006,11.943 C17.796,8.756 15.22,6.173 12.031,6.172 L12.031,6.172 Z M15.423,14.416 C15.279,14.821 14.586,15.19 14.253,15.24 C13.954,15.285 13.576,15.303 13.161,15.171 C12.909,15.091 12.586,14.984 12.173,14.806 C10.434,14.055 9.299,12.304 9.212,12.189 C9.125,12.073 8.504,11.249 8.504,10.396 C8.504,9.543 8.952,9.123 9.111,8.95 C9.27,8.777 9.457,8.733 9.573,8.733 L9.905,8.739 C10.011,8.744 10.154,8.699 10.295,9.037 C10.439,9.384 10.786,10.237 10.829,10.324 C10.872,10.411 10.901,10.512 10.843,10.628 C10.785,10.744 10.756,10.816 10.67,10.917 L10.41,11.221 C10.323,11.307 10.233,11.401 10.334,11.575 C10.435,11.749 10.783,12.316 11.298,12.776 C11.96,13.367 12.519,13.55 12.692,13.636 C12.865,13.722 12.966,13.708 13.068,13.593 C13.169,13.477 13.501,13.087 13.617,12.913 C13.733,12.74 13.848,12.768 14.007,12.826 C14.166,12.884 15.018,13.303 15.191,13.39 C15.364,13.477 15.48,13.52 15.523,13.592 C15.568,13.664 15.568,14.011 15.423,14.416 L15.423,14.416 Z M12,0 C5.373,0 0,5.373 0,12 C0,18.627 5.373,24 12,24 C18.627,24 24,18.627 24,12 C24,5.373 18.627,0 12,0 Z M12.029,18.88 C10.868,18.88 9.724,18.588 8.711,18.036 L5.034,19 L6.018,15.405 C5.411,14.353 5.091,13.159 5.092,11.937 C5.093,8.112 8.205,5 12.029,5 C13.885,5.001 15.627,5.723 16.936,7.034 C18.246,8.345 18.967,10.088 18.966002,11.942 C18.965,15.767 15.853,18.88 12.029,18.88 L12.029,18.88 Z" id="Shape"></path>
	        </g>
	    </g>
	</svg>
</a>


<a href="https://twitter.com/intent/tweet?text=<?php echo get_post_permalink(); ?>">
	<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	        <g id="Group" fill="#000000" fill-rule="nonzero">
	            <path d="M12,0 C5.373,0 0,5.373 0,12 C0,18.627 5.373,24 12,24 C18.627,24 24,18.627 24,12 C24,5.373 18.627,0 12,0 Z M18.066,9.645 C18.249,13.685 15.236,18.189 9.902,18.189 C8.28,18.189 6.771,17.713 5.5,16.898 C7.024,17.078 8.545,16.654 9.752,15.709 C8.496,15.686 7.435,14.855 7.068,13.714 C7.519,13.8 7.963,13.775 8.366,13.665 C6.985,13.387 6.031,12.143 6.062,10.812 C6.45,11.027 6.892,11.156 7.363,11.171 C6.084,10.316 5.722,8.627 6.474,7.336 C7.89,9.074 10.007,10.217 12.394,10.337 C11.975,8.541 13.338,6.81 15.193,6.81 C16.018,6.81 16.765,7.159 17.289,7.717 C17.943,7.589 18.559,7.349 19.113,7.02 C18.898,7.691 18.443,8.253 17.85,8.609 C18.431,8.539 18.985,8.385 19.499,8.156 C19.115,8.734 18.629,9.24 18.066,9.645 L18.066,9.645 Z" id="Shape"></path>
	        </g>
	    </g>
	</svg>
</a>
                        </p>
					
				</header>
				
                
                <div class="events-container">
                    <div class="post-map">
                        <?php 
                            if ( get_field('victim_event') ) {
                                /* if victim is related to a massacre then get that event location */
                                $eventID = get_field('victim_event'); 
                                //echo $eventID[0];
                                $eventMap = get_field('event_location_map', $eventID[0]);
                                echo $eventMap;
                            } else {
                                /* if victim not then get the victim post location */
                                echo get_field('victim_location_map');
                            }
                        ?>
                    </div>
                    <?php the_content(); ?>

                    <?php if ( get_field('victim_event_sources') ): ?>
                        <br>
                        <strong>Fuentes:</strong>
                        <?php echo get_field('victim_event_sources'); ?>
                    <?php endif; ?>

                    <?php if ( get_field('victim_related_articles') ): ?>
                        <br>
                        <strong>Artículos:</strong>
                        
                        <?php 
                            $articles = get_field('victim_related_articles'); 
                            
                            echo '<ul class="victims-list">';
                            foreach ( $articles as $article ) {
                                echo  '<a href="' . get_permalink($article->ID) . '">' . $article->post_title . '</a>'; 
                            }
                            echo '</ul>';
                        ?>
                    <?php endif; ?>

                    <?php if ( get_field('victim_event') ): ?>
                        <br>
                        <strong>Otras Victimas:</strong><br>
                        <?php 
                            $eventID = get_field('victim_event'); 
                            //echo $eventID[0];
                            //var_dump( get_field('event_victims', $eventID[0]) );
                            $eventVictims = get_field('event_victims', $eventID[0]);
                            echo '<ul class="victims-list">';
                            foreach ( $eventVictims as $victim ) {
                                echo  '<a href="">' . get_field('victim_name', $victim) . ' ' . get_field('victim_lastname', $victim) . '</a>'; 
                            }
                            echo '</ul>';
                        ?>
                    <?php endif; ?>

                </div>
				
			</div>
			
		</div>
		<?php
		if ( 'on' === $article_sidebar ) {
			get_sidebar( 'single' ); }
		?>
	</div>
	
</article>

<?php
get_footer();
