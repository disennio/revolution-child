(function($) {

    jQuery(document).ready(function () {
        
        var victima_photo_url;
        if ( $('.acf-image-uploader').hasClass('has-value') ) {
            var victima_photo_url = $('.acf-image-uploader img').attr('src');
            $('#acf-field_61ddb29b7eb2c').val(victima_photo_url);
        }

    });

})( jQuery );
