<?php

/*function wpbsearchform( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
    </div>
    </form>';

    return $form;
}*/

add_action('wp_head', 'head_js_code');
function head_js_code()
{
    ?>
    <script>
        var SITE_URL = "<?php echo get_bloginfo('url'); ?>";
    </script>
<?php
};

if (!is_front_page() && !is_admin()) {
    wp_enqueue_style('select2-styles', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_script('select2-script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), '1.0.0', true);
    
    /* SELECT2 */
    wp_enqueue_style('searchbox-style', get_stylesheet_directory_uri() . '/searchbox.css');
    wp_enqueue_script('searchbox-script', get_stylesheet_directory_uri() . '/searchbox.js', array(), '1.0.0', true);
    
    /* CALENDAR */
    wp_enqueue_style('calendar-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    wp_enqueue_script('calendar-script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), '1.0.0', true);
}


function wpbsearchform($form)
{
    wp_enqueue_style('select2-styles', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_script('select2-script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array(), '1.0.0', true);
    
    /* SELECT2 */
    wp_enqueue_style('searchbox-style', get_stylesheet_directory_uri() . '/searchbox.css');
    wp_enqueue_script('searchbox-script', get_stylesheet_directory_uri() . '/searchbox.js', array(), '1.0.0', true);
    
    /* CALENDAR */
    wp_enqueue_style('calendar-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
    wp_enqueue_script('calendar-script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array(), '1.0.0', true);
 
    $form = get_search_form();
 
    return $form;
}
 
add_shortcode('wpbsearch', 'wpbsearchform');

function wpbsearchform_results($results)
{
    $results = get_template_part('searchform-results');
    return $results;
}

add_shortcode('wpbsearchform_results', 'wpbsearchform_results');

/* MASSACRE POST TYPES */

function evento_post_type()
{
    $labels = array(
        'name'                  => _x('Eventos', 'Post Type General Name', 'text_domain'),
        'singular_name'         => _x('Evento', 'Post Type Singular Name', 'text_domain'),
        'menu_name'             => __('Eventos', 'text_domain'),
        'name_admin_bar'        => __('Evento', 'text_domain'),
        'archives'              => __('Item Archives', 'text_domain'),
        'attributes'            => __('Item Attributes', 'text_domain'),
        'parent_item_colon'     => __('Parent Item:', 'text_domain'),
        'all_items'             => __('Todas', 'text_domain'),
        'add_new_item'          => __('Agregar Evento', 'text_domain'),
        'add_new'               => __('Agregar Evento', 'text_domain'),
        'new_item'              => __('Agregar', 'text_domain'),
        'edit_item'             => __('Editar', 'text_domain'),
        'update_item'           => __('Actualizar', 'text_domain'),
        'view_item'             => __('Ver', 'text_domain'),
        'view_items'            => __('Ver Eventos', 'text_domain'),
        'search_items'          => __('Buscar', 'text_domain'),
        'not_found'             => __('Not found', 'text_domain'),
        'not_found_in_trash'    => __('Not found in Trash', 'text_domain'),
        'featured_image'        => __('Featured Image', 'text_domain'),
        'set_featured_image'    => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image'    => __('Use as featured image', 'text_domain'),
        'insert_into_item'      => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list'            => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list'     => __('Filter items list', 'text_domain'),
    );
    $rewrite = array(
        'slug'                  => 'evento',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Evento', 'text_domain'),
        'description'           => __('Post Type Description', 'text_domain'),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'   			=> 'dashicons-location-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('evento', $args);
}
add_action('init', 'evento_post_type', 0);


/* VICTIMS POST TYPES */

function victima_post_type()
{
    $labels = array(
        'name'                  => _x('Víctimas', 'Post Type General Name', 'text_domain'),
        'singular_name'         => _x('Víctima', 'Post Type Singular Name', 'text_domain'),
        'menu_name'             => __('Víctimas', 'text_domain'),
        'name_admin_bar'        => __('Víctima', 'text_domain'),
        'archives'              => __('Item Archives', 'text_domain'),
        'attributes'            => __('Item Attributes', 'text_domain'),
        'parent_item_colon'     => __('Parent Item:', 'text_domain'),
        'all_items'             => __('Todas', 'text_domain'),
        'add_new_item'          => __('Agregar Víctima', 'text_domain'),
        'add_new'               => __('Agregar Víctima', 'text_domain'),
        'new_item'              => __('Agregar', 'text_domain'),
        'edit_item'             => __('Editar', 'text_domain'),
        'update_item'           => __('Actualizar', 'text_domain'),
        'view_item'             => __('Ver', 'text_domain'),
        'view_items'            => __('Ver Víctimas', 'text_domain'),
        'search_items'          => __('Buscar', 'text_domain'),
        'not_found'             => __('Not found', 'text_domain'),
        'not_found_in_trash'    => __('Not found in Trash', 'text_domain'),
        'featured_image'        => __('Featured Image', 'text_domain'),
        'set_featured_image'    => __('Set featured image', 'text_domain'),
        'remove_featured_image' => __('Remove featured image', 'text_domain'),
        'use_featured_image'    => __('Use as featured image', 'text_domain'),
        'insert_into_item'      => __('Insert into item', 'text_domain'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
        'items_list'            => __('Items list', 'text_domain'),
        'items_list_navigation' => __('Items list navigation', 'text_domain'),
        'filter_items_list'     => __('Filter items list', 'text_domain'),
    );
    $rewrite = array(
        'slug'                  => 'victima',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __('Víctima', 'text_domain'),
        'description'           => __('Post Type Description', 'text_domain'),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'   			=> 'dashicons-universal-access',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type('victima', $args);
}
add_action('init', 'victima_post_type', 0);

/* VICTIMS POST TITLES */

function set_post_title($post)
{
    global $current_user;

    if (get_post_type() != 'victima' || $post->post_status != 'auto-draft') {
        return;
    }

    $count_posts = wp_count_posts('victima');
    $published_posts = $count_posts->publish;

    // Check if it's the first one
    if ($published_posts == 0) {
        $title = '1';
    } else {
    
            // Get the most recent post
        $args = array(
                'numberposts' => 1,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'victima',
                'post_status' => 'publish'
            );
        $last_post = wp_get_recent_posts($args);
        // Get the title
        $last_post_title = $last_post['0']['post_title'];
        // Get the title and get the number from it.
        // We increment from that number
        $number = $last_post_title;
        $number = $number + 1;
    
        // Save the title.
        $title = $number;
    } ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#title").val("<?php echo $title; ?>");
        $("#title").prop("readonly", true); // Don't allow author/editor to adjust the title
		console.log('este es: <?php echo $title; ?>');
    });
    </script>
    <?php
} // set_post_title
add_action('edit_form_after_title', 'set_post_title'); // Set the post title for Custom posts


/* DEPARTAMENTOS */

function action_function_name_1($field)
{
    if ($field['key'] == "field_61a69deb069be" || $field['key'] == "field_61a7297ef75b0") {
        include_once "select-department-options.php";
    }
}
add_action('acf/render_field', 'action_function_name_1', 10, 1);

/* CITIES */

function action_function_name($field)
{
    if ($field['key'] == "field_61a69eba069bf" || $field['key'] == "field_61a72993f75b1") {
        include_once "select-city-options.php";
    }
}
add_action('acf/render_field', 'action_function_name', 10, 1);

/* SHORTCODES */

add_shortcode('test', 'exp_post_slider_shortcode');

function exp_post_slider_shortcode($atts)
{
    $a = shortcode_atts(array(
    'post_type' => 'evento',
    'posts_per_page' => '3',
), $atts);

    $output = '';
    $args = array(
            'post_type' => 'evento',
            'posts_per_page' => $a['posts_per_page'],
);
    $post_slider = new WP_Query($args);

    if ($post_slider->have_posts()) {
        // The Loop
        $output .=  '<div class="exp-post-slider-container">';
        $output .= '<div class="owl-carousel owl-theme exp-post-slider">';
        while ($post_slider->have_posts()) {
            $post_slider->the_post();
            $feat_image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
            $output .= '<div class="exp-cat-slide" style="background-image:url('.$feat_image_url.'); background-size:cover; background-repeat:no-repeat;">';
            $output .= '<div class="exp-slide-post-info">';
            $output .= '<h2>' . get_the_title() . '</h2>';
            $output .= '<p><a href="' . get_permalink() . '" class="exp-post-link-btn">Ver Evento</a>';
            $output .= '</div></div>';
        }

        wp_reset_postdata();
    } else {
        $output .= '<div class="exp-cat-slide" style="background-image:url(https://webclient.co/explore/wp-content/uploads/2019/04/looking-out-no-posts.jpg); background-size:cover; background-repeat:no-repeat;">';
        $output .= '<div class="exp-slide-post-info">';
        $output .= '<h2>No Results</h2>';
        $output .= '<p>Please try again</p>';
        $output .= '<p><a href="https://webclient.co/explore/blog/" class="exp-post-link-btn">Check Out Our Blog</a>';
        $output .= '</div></div>';
    }


    $output .= '</div>';
    $output .= '</div>';

    return $output;
}



add_shortcode('victimas', 'exp_post_slider_shortcode_victimas');

function exp_post_slider_shortcode_victimas($atts)
{
    $a = shortcode_atts(array(
    'post_type' => 'victima',
    'posts_per_page' => '10',
), $atts);

    $output = '<div id="content"><div id="results-container"><div class="results-controls"><h2>Resultados</h2><a href="http://localhost:10044/wp-content/uploads/files/export.xlsx" class="export-results" ><svg width="12px" height="15px" viewBox="0 0 12 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Website" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="Website-/-Victimas---Caja-llena-Copy" transform="translate(-772.000000, -481.000000)"><g id="Group-15" transform="translate(123.000000, 471.500000)"><g id="Group-13" transform="translate(637.000000, 0.500000)"><image id="Bitmap" x="12" y="9" width="12" height="15" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWAAAAHACAYAAACcfbKiAAAABGdBTUEAALGOfPtRkwAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAABYKADAAQAAAABAAABwAAAAAD9GqWcAAAmGklEQVR4Ae2defBkV1XHZ7IQsk2YMCEJiCUkkEgoDKJRlmBACFvAIEGrqPIPQJaKSxBRSqAsBBXUBASDZZRgyWIFRDQoYagyRcAkf6iEpQCDEzKRADNJJoEME0ICGL9npnumf/3r5fZ7775z3z2fW3Wmt/vuOedzzvvO+73ufr1xAwMCwyFwrkI9T3aabMvI7tPtrpF9Sbcfll0uY0AAAhCAQEsCm7X922V7ZCa2KbZb8y6UbZIxIAABCECgAYELtM0dshTRnTXndm17fgO/bAIBCEAgLAE7cr1SNktUmzy3VWtxNBy2nUgcAhBIJXCSJt4gayK0i7bZpjUflhoE8yAAAQhEI2Dne7fLFglpm9du1NrmgwEBCEAAAhMEDtX9a2VtBDZl26vlw3wxIAABCEBgROANuk0R0C7mvBbqEIAABCCwj8BxurlL1oW4pqxxp3wdu881/0IAAhCITeASpZ8inF3OuTg2crIvgcDGEoIghtAEjlD29sWJg3umcI/8HSOzWwYEXAgc5OIVpxA4QOAZutu3+Jr3w2Rn2x0GBLwIIMBe5PE7JnDO+I7D7XMcfOISAvsJIMD7UXDHicDpTn7Nradvx7RxXQoBBLiUSsSN40GOqXv6dkwb16UQ4E24UioRN457lbrXFyPsCmtHx0VP5t4EEGDvCuDfPlrmOdgHPOkH980piOANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfAQTYjz2eIQCB4AQQ4OANQPoQgIAfgUP8XLfyfKK2PmlkD9ftybKHyDbJjhzZUbo9Rlb6uFsB7hrZl3T7YdnlMgYEuiZwrhY8T3aabMvI7q/b0sedCnCP7K6R7dbt12U3yG6UfXVkO3XLyEDgZ7Tmb8s+KrtDdl/lZg12ocz+Q6l9eNeydr6bleDbZSZg3qxz+zdtMI14tewMGaMhATtyfbHsEzI7QsxduFLXv125ny+reXizr5ntBUouwgHLvB4y7fi4zLRkCH8NK0y/YacOXiSz/8Hukc2DGvH5reJR69Gwdz2FtrphvXKlzJttSf6/Jx7/JPsl2eEyxojAsbp9q+w7spIKVlos28TnYbLahjfn2njaeyN2btSba8n+vyU+b5Q9QBZ2mPC+RbZbVnKxSorN3nCwc3o1DW++NbG03tgu82Y6FP/fFqs3yUIJsX3y4tUye2dzKIUqKc6rxe1QWS3Dm20tHK0nrpV58xyifzsi/nXZwbKqxxOV3f/IhlikkmJ+bUVd4s21FpRvYL9qrSv2MdAn1NIQk3nY53Y/JPPe2Wrxb3892CmcGoZ3TWpgeJySsM/IerOsxf/7xfKEGhrDcrB3HXmDrfud4+JKGsR7p60B4yVKwptjbf7t/PC5Q24O+4bNu2mMbDuGfazmsCE3yCh27x136AiPUAI/kHlzrNX/u8R2cPvZKQr6epoi+07xXDEe+vDecYfO7/lKwJth7f6/KMb28b7OR46L8bxEUV4nMxFm5CXwnLzLs/oACJwzgBiHHqJdO+MLMjud2unoWoD/TNFdKrM/ixj5CZye3wUeCidAD/RTINO0D8r+oB93q3v5C21S+58ipeVnX8wY+vBmOnR+NysBb4bR/NuBZlHjrxVNtCKUkK99umTow5vj0PndqwS8GUb0bwecrUcXpyDsIzAvax0JCzQhsLHJRmxTFQETYEb/BOybc3/V1m1bAf5DBfDytkGwfWMCXIC6MbpqNry1mkyGl8grFPKb24TdRoBfKcevb+OcbVsTQIBbIxz8ArcMPoNhJ2BfAf+1pik0FeAXyOFfNnXKdp0R+GxnK7HQUAl8bqiBVxS3nQ9+YZN8mgjwWXJ0mYzzj02Id7vNFd0ux2oDJLB1gDHXFrJp4d/LnrpqYquK6I/JgX0g+ehVHTG/cwL25ovVYehvwtg76J5j1X3AM9ZZvo/Uk3ZxpuovpTgr+cKes08lPUZ2U2pcqxwB2/VG/1mG+KbSzTvvPVp+6OKbl1CM1e0qaO+NkWrxWZo22s8e3S9HpO/UohE/71dizvZjg8fnKLLDmt58HVLu3OWPaEX7z9ibJf731eDPu67wMyluUc39J10X2HE9753WMfVOXV+k1bxZ4v9ADZ6dUt2U818P0EI3yB6YsiBzshOwCx09SWZHwTUM22k9R8o+4Blfqm87F2w/SWTnIBn+BHYphEfIvr0olJRzwPa9Z8R3EcX+XvtfuXqGrBbx7Y9c/Z7sXLD1xtfrT3UQGW5RlG9pG+njtQB/VpTB4CbV4uS2BS1we+/+KhBJq5DsqOsmmTdX/O+rwU83raZ9rIWLqpfRyFepFpubFrLw7bx31MLxNArPeuUamTdb/O/72G6j01yvoYDuDWzf87drbaScKtK0QQ7vnXSQ0BKCtgOoV8huk3kzju7fLtyz0jhKs3fLooPzyt/Y27vam2S1Dy/GY7+187U30d8h2yMb58xtvyzuEHu7oHvy+B3NpEj9MPiuWH9NZp9usK8zPk8WaXj3WSTW1lt2GQG7hsjNMnsz15t/FP8XiPW6Me/cxDc188R1s8t9wkTskzI77/WNkVkO1mT2Pz+jXAK2A3qOefuAZ0z4PkDA/hp/qOzBsoeM7Im6farscNlQhumSfWFm6XipZgzhfyU7arQ/05+2NCMmlEzAu9dKZkNsiwmcrZffJrMDLe8+SvH/4sXp7LvC2fbCk7E3FV4lu9+yZHh9EARSGjfnnEFAIsiFBEwLfktmX37I2Stt1/6K4lv4F9fPF5yAfdDcfoHDLnjBqIdA26Zuu309JMlkkxD8kcy0om1f5NreNHbueLdeyeW4zbrbFVeNX0KYW4hAL7Tpiy62DYQ6TKqmFaYZXfRH12v8zbwqHKoX7HqWXTtsu95ViqnWLyEotfCjbX+03T58ASoFUOqXUW4X74NnMX+unmzbzF1vb9e8nRnsrAR4bpAEuu6ZVdcbJDSCTiJwiGa9X7ZqT+SeP/NKaR8oLFCLh1E/gdzNvmz9+gmT4fuEYFkf9Pn6e2eVpKTTD9cqQDslwqifQJ+NP8tX/YTJ0LTkatms+ns8Z5/WWDMerUcegczyuV2xcM53TXmqfjCrB/p8rmq4JLefgGmKfXegz95a5OtUi+wg+0fDLvBdwrCPj9iHq79VQjDEAAEIVEPANMV+2aeUb8aeaWRLE+DXKaZtFhgDAhCAQMcEvqz13tjxmk2X2yvA441v0p1Fh8t9vHazYuC877gicW776K1FPuKQJlMjcH/ZDtminujjte0WjA379kgfDpf5eNneaPgnGoFlfZH79Wi8yXfDhvMFIXdfpaxvlwrd8LgCgrlRMfB5X6tGvJHSqDnnxCNOxnbtiBLekPtJOwd8cgH1sK9A/7CAOAgBAhCon8C9SvHSAtI8yQT4pAIC+XgBMRACBCAQh8AVBaS6V4C9j4DtQ8l2hX4GBCAAgb4I/Jcc2aVtPcdeAT7BMwL5/pizf9xDAALxCNj7Clud0z7RTkHYpyA8B0e/nvTxDYG4BK5zTv1oE2DvC5zbZ/IYEIAABPomsLNvh1P+EOApIDyEAATiEPA++NtUwikIbwhx2o1MIQCBSQLe2nO0/UCcnYz2HEfKuf2sPCMmAe/+W/gjiTFLEiZrO/262zHbu0sQYHYAxw4owDUCXEARAofg2n92CoIBAQhAAAIOBBBgB+i4hAAEIGAEEGD6AAIQgIATAQTYCTxuIQABCCDA9AAEIAABJwIIsBN43EIAAhBAgOkBCEAAAk4EEGAn8LiFAAQggADTAxCAAAScCCDATuBxCwEIQAABpgcgAAEIOBFAgJ3A4xYCEIAAAkwPQAACEHAigAA7gcctBCAAAQSYHoAABCDgRAABdgKPWwhAAAIIMD0AAQhAwIkAAuwEHrcQgAAEEGB6AAIQgIATAQTYCTxuIQABCCDA9AAEIAABJwIIsBN43EIAAhBAgOkBCEAAAk4EEGAn8LiFAAQggADTAxCAAAScCCDATuBxCwEIQAABpgcgAAEIOBFAgJ3A4xYCEIAAAkwPQAACEHAigAA7gcctBCAAAQSYHoAABCDgRAABdgKPWwhAAAIIMD0AAQhAwIkAAuwEHrcQgAAEEGB6AAIQgIATAQTYCTxuIQABCCDA9AAEIAABJwIIsBN43EIAAhBAgOkBCEAAAk4EDnHyG9XtuUr8PNlpsi0ju0+3u0b2Jd3+g+yjMgYEuiZA/3VNtIP1TAA8rYMUil5is6J7m2yPLJXzbs29ULZJVvtIZZJrXu186b/FFc7VV6nrJotC6oKrzluMZ9ivXqDw75CtymQ8/3Zte/6wESyNfpyr1+3SAAc8gf5bXjyvvhv7bSwO4wXa3i5HNLwZduR6pawtm/H2W7VWrUfD4xy9bofXXcsjpv+WMxrP8Oq7sd/ORGK84Kq3YxC13J6kRG6Qrcph2fxtWtPWrm0syzv367XxfIQSytV/D6sNlvLJ3V/L1vcPoKKi2vm27RmLukNrn1oRL0tlWYPmfr0mnNYbt2ZkeqPWth6vaeTur2XrswN01E2Hap1rZcuAt329NhFuy6Pt9h2V332Z3OI75ny1MrVer2WM8/K6zS4YyxKrpZBvUCLLcu3q9ZpEuCsmTdepof/6Et8x49fWAG2Uwzgnr9veRGNegjXU8jglcZdsXo45nq9FhHOwWWXNofdf3+JrbO+UHTt0cKP4V+mVHHN7FY1ZCdRQx0uUxKzccj9XgwjnZrRs/SH3n4f4jnlePGRwE7GP8/G6dRGOyWQnWAzy7hGK+geyyZz6vD90Ee6T1Sxfg2w6Be0pvsbxe7LDhgpvIu5ZPdHbc1wLYqISDe8+Q9sd3HDbLjY7QYt8UmY7JCMGAav1p2V26strmPie7eW8Fr8IcPtKntN+idYrIMKtEQ5mgRLEdwzrOeM73DYjgAA34za51emTDxzvI8KO8HtyXZL4Wsql9H5P+Lt3gwC3Z/qg9kt0tgIi3BnK4hYqTXwNUEm9X1zBUgLaqEl2wtlzWAxDHvcq+NI+mL5TMT1Fdv0AwNJ/y4tUovha1HaFv6OXh1/0DNf+4wi4fW+4FnBO+BwJzwEzwKdLFV9DOfSDJ/d2QIDbl+Cb7ZfIsgIinAVrr4uWLL4Gwv7SYrQggAC3gDfa9Jb2S2RbARHOhjb7wqWLrwFAgFu2AQLcEqA2/1z7JbKugAhnxZtl8SGIryX+2SzZB1oUAW5f7CvaL5F9BUQ4O+LOHAxFfC3hIfR+Z4XJsRCfgmhP9UgtYT8dNISvZdqfjKV9OsL7TcyS3kgakvjap3/sExB2O+Th2n8cAbdvHbsK2vvbL9PLChwJ94K5kZMhia8l+B7Z0MW3UaG63sj+B/C0rvPxWO+hcnqPM8dVarhDsdoOX8JYJe4cc0tgYLXI+UsWXXO7W/EeXwK4DmLoms1K63EE3EEFtcTNsnd1s1Qvq3Ak3AvmJCdDO/K1pN4pK/nTP0ngS5m0kmIr6K7nl8KhbRx2LvjzGfh0zXtyvRKOhCfj8bjftu5tth/aka/V5zOyw9skXdi2Hj036bNzQZ1cPOV+YfVoFY4dWdrRcErepczxFmFvDq0K3mLjIYrvTcp3S4ucS9zUvf/cAyixKi1iOkXb3ibz5rqK/52K1wTBY6wSZ465Hjkba/sTPkc+uda0HnmkB6zMPnPxSl3XvQky83VZ3nawIb2pYs3idSSc2qi55vXdIPRG38QX+8vVV6nrIsCL69P4VXa0NHSpjZprXlqU3cyiJ7rh2OUqufoqdV0EuMtqTq3FDjcFZMbD1EbNNW9GSFmeoheyYG29aK6+Sl0XAW5dwsULsOMt5pPaqLnmLY6um1fpgW445lglV1+lrosA56jq1JrsgFNAJh6mNmqueROhZLlL7bNg7WzRXH2Vui4C3FkpFy/EjjibT2qj5po3O6punqXm3XDMuUquvkpdFwHOWd2ptdkhp4DoYWqj5pq3PqJunqHW3XDMvUquvkpdt9odIHfhmq7PjrmWXGqj5pq3NppuHlHjbjj2sUquvkpdFwHuo8pTPthBDwBJbdRc8w5E0s09atsNx75WydVXqesiwH1VesoPO+o+IKmNmmveVFlaPaSmrfC5bJyrr1LXRYBdyr7PKTtsPeeAqaXjjtTCdapQ5pqHALcoXhebRt9xczV26rrUsAsCw10jtU9yzUOAC+idyCKcq7FT121b/si1a8uuhO1T+yTXPAS4hC5QDFF35FyNnbpum/JHrVkbZqVtm9onueYhwAV1RMQdOldjp67btPwRa9WUVcnbpfZJrnkIcGHdEW3HztXYqes2KX+0GjVhNJRtUvsk1zwEuMBOibSD52rs1HVXLX+k2qzKZojzU/sk1zwEuNCuibKj52rs1HVXKX+UmqzCZOhzU/sk1zwEuOAOirDD52rs1HVTyx+hFqksapqX2ie55iHAhXdT7Tt+rsZOXTel/LXXIIVBrXNS+yTXPAR4AJ1VswDkauzUdZeVv2b2y3KP8Hpqn+SahwAPpMtqFYJcjZ267qLy18p8Uc7RXkvtk1zzEOABdVyNgpCrsVPXnVf+GlnPyzXy86l9kmseAjyw7qtNGHI1duq6s8pfG+NZOfLcPgKpfZJrHgI8wE6sSSByNXbqutPlr4ntdG48Xk8gtU9yzUOA19dkEM/UIhS5Gjt13cli18J0MifuLyaQ2ie55iHAi+tT9Ks1CEauxk5dd1zgGliOc+E2nUBqn+SahwCn16rImUMXjlyNnbquFXXoDItszIEEldonueYhwANplEVhDllAcjV26rpDZreoJ3gtjUBqn2SZt1Ex2sKew2JgtCdgQvLvsi3tl+pthVvk6fjevM12dKueftDsl4p81pidJbu+yOiGF5Sr/iHAw2uYRRGbCH9adtyiSbw2WAI7FflTZIhvdyV0FeCDusuDlQogYDvmk2W3FRALIXRLAPHtlmcRqyHARZSh0yAQ4U5xFrEY4ltEGboPAgHunmkJKyLCJVShmxgQ3244FrkKAlxkWToJChHuBKPrIoivK/78zhHg/Iw9PSDCnvTb+UZ82/EbxNYI8CDK1CpIRLgVPpeNEV8X7P07RYD7Z+7hERH2oN7MJ+LbjNsgt0KAB1m2RkEjwo2w9boR4tsrbn9nCLB/DfqMABHuk/ZqvhDf1XhVMRsBrqKMKyWBCK+Eq5fJiG8vmMtzggCXV5M+IkKE+6Cc5gPxTeNU5SwEuMqyJiWFCCdhyjoJ8c2Kt/zFEeDya5QzQkQ4J93FayO+i/mEeBUBDlHmhUkiwgvxZHkR8c2CdXiLIsDDq1mOiMcivCvH4qy5hoBdz5dLSq5BEvcBAhy39tOZmwifKeNSltNkuntsR75nyYw1AwIbEGCaYJLA+EgYEZ6k0s19Tjt0w7GqVRDgqsrZSTKIcCcY1yyC+K7BwYMxAQR4TILbSQKI8CSNdvcR33b8qt4aAa66vK2SQ4Rb4du7MeLbnmHVKyDAVZe3dXKIcHOEiG9zdmG2RIDDlLpxoojw6ugQ39WZhdwCAQ5Z9pWTRoTTkSG+6azCz0SAw7dAMgBEeDkqxHc5I2ZMEECAJ2BwdykBRHg+IsR3PhtemUMAAZ4DhqfnEhiL8K1zZ8R7ga8Xx6t5JxkjwJ1gDLeIifDPyfjG3IYNduR7lsyYMCCwEgEEeCVcTJ4gMD4SjizCnHaYaAjuNiNwnzbztGZRs1UpBE5VIHY6wrOHPHzvUM6WO2PYBDx6Z9Kn+44z7PIRvRGIJsKIbz19PymGHvcR4Hp6yTWTKCKM+Lq2WefOPUR30icC3HlJ4y5YuwgjvvX19qQYetxHgOvrKdeMTITtjTmPZs7pc6dystwYdRHI2TMpa7vvKHWVk2yMQG1Hwhz51tvXKSKZcw4CXG9vuWZWiwgjvq5tlN15TnFNWRsBzl7iuA5MhO1bYimNWOKcnYrdcmDUS8C779x3jnpLS2ZGYKgijPjG6F8EOEadQ2dpIjykL2tw2iFOuyLAcWodOtOhiDDiG6tNEeBY9Q6dbekijPjGa08EOF7NQ2dcqggjvjHbEgGOWffQWZcmwohv3HZEgOPWPnTmpYgw4hu6Df0/Beb+P0Ds+ofO3luEEd/Q7bc3eXf9cw+AHghNwEuEEd/Qbbc/eXf9cw9gPwruRCXQtwgjvlE7bX3e7vrnHsB6JjwTkEBfIoz4BmyuBSm76597AAvg8FIsArlFGPGN1U8p2brrn3sAKZSYE4bAI5XpTbKu+9LWtLUZEJgk0HWfrbpe542+cgCTNLgPARHYLLtGtmovzZt/1WhN3TAgsIbAvJ7p6/nOmrxpwGto8AACIwIH6/YVsja/rmEXAHq57CAZAwKzCDTVra62Q4BnVYXniiHwAEXyDtkeWWrT79bci2SbZAwILCKQ2lNZ5m1UZLaw57AYGBBIIfA8TXqR7BTZlpFZ/+4a2fW6vUz2URkDAikEXPUPAU4pEXMgAIFaCbgKMOfGam0r8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIongAAXXyIChAAEaiWAANdaWfKCAASKJ4AAF18iAoQABGolgADXWlnyggAEiieAABdfIgKEAARqJYAA11pZ8oIABIonYAL8fecoj3T2j3sIQCAmgaOc0/6+CfBdzkGc4Owf9xCAQEwC3tqzxwR4jzP745394x4CEIhJoAgB9j4CRoBjNj9ZQ8CbAAKsCnhD8G4C/EMAAj4ETvRxu9/r3lMQ3kfAj9sfDncgAAEI9EfAW3v2CvCO/vKd6ekX9OzGma/wJAQgAIE8BExznp9n6eRVd9ibcNuSp+eZuEXLPj7P0qwKAQhAYCaBM/Xsppmv9PfkthIE2NK1o2AGBCAAgb4IlKA5ew9+n6CM73O2XfLPFzL6aj38QCA2AdOa22XeundGKUfADxSMV8XuCbKHAAR6IvAa+Tm2J1+L3Hx5/OI3dMf7f4NvK4ZjxgFxCwEIQCADARNe+/KZt95tt9zsCNjGp/bduP5r4vs61whwDgEI1E7g9UqwhNOdn54E/Uo98P4fwfzfK/vZycC4DwEIQKAjAmdonR/IStC6X53M6VGFBGVgbpE9eDI47kMAAhBoScC+9XarrATxtRgeOZ2PfRKhlOA+o1gOnQ6QxxCAAAQaEDhM21wnK0Xf7BMY68bf6ZlSArQ43rMuQp6AAAQgsDqBy7RJSdp26awUnlVYkGMRPmRWsDwHAQhAYAkB0w47kCtJfC2Wp8+K+2A9eUeBwV6jmDbPCpjnIAABCMwhYN8tMO0oTXxNY01rZ45L9GxpAVs822WnzoyYJyEAAQisJfBoPfyarEQtu3htqGsfnVVo0Abyu7I/lh0tY0AAAhCYJmAX1/lT2d2yEsXXYnqSbO6wS7RdLys1eIvrNtlvyviUhCAwIACBDfcTg1fLSri+wyLt/IpiNI1dOF6sVxctUspr9rE5++TGC2QlfLNFYTAgAIGeCNg+/0LZ+2SlC+9YM39lms08Nf6GJg7tyxBbFbN9vc9iH9vNuu/9ix8KgQEBCDQkYKcVHiIzPbJbs7NkZ8uGNEyLfnQ64HkCbIfzF01P5jEEIAABCDQicIG2euf0lvME+HBNtKNIPv41TYzHEIAABFYj8C1NtyN3e3NwzZj3eTS7YIXZ0A7z1yTHAwhAAAIFEPg9xXD1rDjmHQHbXBPnL8r4/K3RYEAAAhBYnYB98uE02Q9nbXrQrCdHz9kGay6ZtmAuL0EAAhCAwHoCL9FTM8XXpi4SYHv9GtkH7Q4DAhCAAARWIvABzb520RaLTkGMtztRd26QHTF+glsIQAACEFhI4Dt69eEy+77C3LHsCNg23CH7jbkr8AIEIAABCEwTsFMPC8XXNpj3KYjpxT6rJ+xXM+xkMgMCEIAABOYTsEtgvnX+ywdeSTkFMZ5tpyD+U2ZCzIAABCAAgfUE/ltPPVZ2z/qX1j+zigDb1ifLPi/jfLDRYEAAAhA4QMCu2PgTMnvPLGmknAOeXMgWtovfzP1YxeRk7kMAAhAIQsC+uPZ8WbL4GpfUc8A2dzzMwY0yc7bqEfR4DW4hAAEI1ELArnb2y7J/XTWhJgJsPr4gs3f4nm0PGBCAAAQCEzhfub+vSf5NBdh82Rty9nPPZ9oDBgQgAIGABN6knC9smncbATafV8pOkP2UPWBAAAIQCETAft/td9vk21aAzffHZHYu+CwZAwIQgEAEAnbk20p8DVIXAmzrXCX7puwcGW/MCQIDAhCokoC94WYXKXt7F9l1LZa/qKAukx3aRXCsAQEIQKAgAt9XLPY7dJd3FVPXAmxxnSH7iMyuAM+AAAQgUAMB+4UgO8D8jy6TWfWLGCm+LcDHyD6VMpk5EIAABAon8G+KzzStU/G1nLs6B2xrTQ777aP3jp54sm5zHGlP+uM+BCAAga4J/J8WfKPsZbJ1v+em51qPPoTxaYrSLup+bOtoWQACEIBAPwRukxs735v1L/kcpyCm8djh+ymyv5XZO4gMCEAAAqUSMI26VGa/hZlVfA1AH0fA5mc8Hqc7lpxdMYgBAQhAoCQCn1EwL5XZFR97GX0cAU8mYgk+VnaezK4nwYAABCDgTeBzCsCu8mjf6O1NfC3pvo+AzefkOFcPfl9mosyAAAQg0CeB6+TsTbLL+3Q66ctbgMexPE93TIjtFAUDAhCAQE4CdiGxN8v+JaeTlLVLEeBxrKfrjv0pYPbj4ye5hQAEINCSwJe1/T/KPiKzUw5FjNIEeBLKo/TATlHY9SUeP/kC9yEAAQgkELhWc66QfUi2LWF+71NKFuBJGJv14OmyZ8meKbNLYDIgAAEITBLYoQdbR/YJ3d45+WKJ94ciwNPsTIAfLrMfCTU7SWbXntgkO1J21Oj2GN0yIACBYRMwId0ju2tku3X7ddlXRzb+mbSdejyo8f+73CT/S+GN0AAAAABJRU5ErkJggg=="></image></g></g></g></g></svg> Exportar Datos</a></div><div id="results">';
    $meta_query = array('relation' => 'OR');
    $args = array(
        'post_type' => 'victima',
        'posts_per_page' => $a['posts_per_page'],
	);

    if (isset($_GET['victim_age'])) {
        $meta_query[] = array(
            'key' => 'victim_age',
            'value' => $_GET['victim_age'],
            'compare'      => 'LIKE'
        );
    }

    // allow the url to alter the query
    if (isset($_GET['victim_name'])) {
        $meta_query[] = array(
            'key' => 'victim_name',
            'value' => $_GET['victim_name'],
        );
    }

	// allow the url to alter the query
    if (isset($_GET['victim_lastname'])) {
        $meta_query[] = array(
            'key' => 'victim_lastname',
            'value' => $_GET['victim_lastname'],
        );
    }

    // allow the url to alter the query
    if (isset($_GET['victim_gender'])) {
        $meta_query[] = array(
            'key' => 'victim_gender',
            'value' => $_GET['victim_gender'],
        );
    }

	// allow the url to alter the query
	if (isset($_GET['victim_date_one'])) {
		$meta_query[] = array(
			'key' => 'victim_date',
			'value' => $_GET['victim_date'],
		);
	}
	// allow the url to alter the query
	if (isset($_GET['victim_date_two'])) {
		$meta_query[] = array(
			'key' => 'victim_date',
			'value' => $_GET['victim_date'],
		);
	}
    
	if (!empty($meta_query)) {
        $args['meta_query'] = $meta_query;
    }

    if ($_GET['test']) {
        echo '<pre>';
        print_r($args);
        echo '</pre>';
    }
    $post_slider = new WP_Query($args);

    if ($post_slider->have_posts()) {
        // The Loop
     
        while ($post_slider->have_posts()):
        
        $post_slider->the_post();
		if(get_field('victim_photo', $post->ID)){
			$feat_image_url = get_field('victim_photo', $post->ID);
		} else {
			if(get_field('victim_gender') == 2){
				$feat_image_url = get_template_directory_uri().'/assets/img/user-2.png';
			} else {
				$feat_image_url = get_template_directory_uri().'/assets/img/user.png';
			}
		}
		
        
        $output .= '<a href="' . get_permalink() . '" class="result-victim">';

        $output .= '<div class="result-victim-img" style="background-image:url('.$feat_image_url.'); background-size:cover; background-repeat:no-repeat;"></div>';
        $output .= '<p>' . get_field('victim_name') . ' ' . get_field('victim_lastname') . '</p>';
        $output .= '<p>' . get_field('victim_date') . ' - ' . get_field('victim_location_city') . ', ' . get_field('victim_location_department') . '</p>';
        $output .= '<div>';
        $victim_perpetrators = get_field('victim_perpetrators', $post->ID);
        if ($victim_perpetrators) {
            $output .= '<p class="result-item-multiple">' . implode(', ', $victim_perpetrators) . '</p>';
        }
        $output .= '</div>';
        $output .= '<div></div>';

        $output .= '</a>';
    
        endwhile;

        wp_reset_postdata();
    } else {
        $output .= '<div class="exp-cat-slide" style="background-image:url(https://webclient.co/explore/wp-content/uploads/2019/04/looking-out-no-posts.jpg); background-size:cover; background-repeat:no-repeat;">';
        $output .= '<div class="exp-slide-post-info">';
        $output .= '<h2>No Results</h2>';
        $output .= '<p>Please try again</p>';
        $output .= '<p><a href="https://webclient.co/explore/blog/" class="exp-post-link-btn">Check Out Our Blog</a>';
        $output .= '</div></div>';
    }

	$output .= '</div></div></div>';
    return $output;
}

// Google Maps

function my_acf_google_map_api($api)
{
    $api['key'] = 'AIzaSyCuw5YY-b3TCbuwgNpfa-HudIc4vS2cR0I';
    
    return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');



function child_remove_parent_function()
{
    remove_action('init', 'thb_preloader');
}
add_action('wp_loaded', 'child_remove_parent_function');
/* Preloader */
function thb_preloader_2()
{
    if (in_array(ot_get_option('thb_preload_type', 'preloader'), array( 'preloader', 'preloadlazyload' ), true)) {
        $thb_preload_icon = ot_get_option('thb_preload_icon', 'preloader-material'); ?>
	<div class="thb-page-preloader">
		<?php if ('preloader-line' === $thb_preload_icon) { ?>
			<div class="preloader-style3-container"></div>
		<?php } else { ?>
			<?php get_template_part('assets/img/svg/' . $thb_preload_icon . '.svg'); ?>
			<?php if ('preloader-three-circles' === $thb_preload_icon) { ?>
			<div class="preloader-style2-container">
				<div class="thb-dot dot-1"></div>
				<div class="thb-dot dot-2"></div>
				<div class="thb-dot dot-3"></div>
			</div>
			<?php } ?>
		<?php } ?>
	</div>
		<?php
    }
    if ('thb-swipe-left' === ot_get_option('page_transition_style') && 'on' === ot_get_option('page_transition', 'on')) {
        ?>
		<div class="thb-page-transition-overlay"></div>
		<?php
    }
}
add_action('wp_body_open', 'thb_preloader_2');





function my_pre_get_posts($query)
{

    // do not modify queries in the admin
    if (is_admin()) {
        return $query;
    }
    
    $meta_query = array();
    // only modify queries for 'event' post type
    if (isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'victima') {
    
    
    	// allow the url to alter the query
		if (isset($_GET['victim_age'])) {
            $meta_query[] = array(
    			'key' => 'victim_age',
    			'value' => $_GET['victim_age'],
    		);
        }
    
        // allow the url to alter the query
		if (isset($_GET['victim_name'])) {
			$meta_query[] = array(
				'key' => 'victim_name',
				'value' => $_GET['victim_name'],
				'compare' => 'REGEXP',
    		);
        }
		// allow the url to alter the query
		if (isset($_GET['victim_lastname'])) {
			$meta_query[] = array(
				'key' => 'victim_lastname',
				'value' => $_GET['victim_lastname'],
				'compare' => 'REGEXP',
    		);
        }

		// allow the url to alter the query
		if (isset($_GET['victim_date_one'])) {
			$meta_query[] = array(
				'key' => 'victim_date',
				'value' => $_GET['victim_date'],
    		);
        }
		// allow the url to alter the query
		if (isset($_GET['victim_date_two'])) {
			$meta_query[] = array(
				'key' => 'victim_date',
				'value' => $_GET['victim_date'],
    		);
        }

    }

    if (!empty($meta_query)) {
        $query->set('meta_query', array(
			'relation' => 'OR', 
			$meta_query 
		));
    }

    // return
    return $query;
}
    
    //add_action('pre_get_posts', 'my_pre_get_posts');


/**
 * ADD CODE TO EDIT CUSTOM POST SECTION
 */

function add_admin_scripts($hook)
{
    global $post;

    if ($hook == 'post-new.php' || $hook == 'post.php') {
        if ('victima' === $post->post_type) {
            wp_enqueue_script('admin-victimas-js', get_stylesheet_directory_uri().'/admin-victimas.js');
        }
    }
}
add_action('admin_enqueue_scripts', 'add_admin_scripts', 10, 1);


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
