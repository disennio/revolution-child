// Form Actions

jQuery(document).ready(function () {
    /*jQuery(".main-form-vidas").submit(function(){
        return false;
    });*/
    jQuery(".advance-search-button").click(function(event){
        event.preventDefault();
        jQuery(".main-form-vidas").toggleClass("show-advance-search");
    });

    jQuery(".form-main-button").click(function(){
        
        // add loading color gradient to header
        jQuery("body").addClass("is-searching");

        // delay
        /*window.setTimeout(function(){

            // redirect to results - change to an API call later
            window.location.href = SITE_URL + "/resultados";

        }, 1500);*/
        
    });

});

jQuery('.main-form-vidas input[required]').on('input propertychange paste change', function () {

    var empty = jQuery(".main-form-vidas").find('input[required]').filter(function () {
        return this.value == '';
    });

    //jQuery(".form-main-button").prop('disabled', (empty.length));

});


// Datepicker

jQuery( function() {
    
    jQuery('#date1').datepicker({
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-100:c+0',
        onSelect: function(selected) {
            jQuery("#date2").datepicker("option","minDate", selected)
        }
    });
    
    jQuery('#date2').datepicker({
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-100:c+0'
     });
});



// Select2

jQuery(document).ready(function() {
    jQuery('.main-form-vidas select').select2({
        minimumResultsForSearch: -1,
        theme: "vidas",
        placeholder: function(){
            jQuery(this).data('placeholder');
        }
    });
});

jQuery('.gender-select').on('select2:select', function (e) {
    var data = jQuery(e.currentTarget).val();
    jQuery('.gender-hidden').val(data);
    console.log(data);
});

jQuery('.event-type-select').on('select2:select', function (e) {
    var data = jQuery(e.currentTarget).val();
    jQuery('.event-type-hidden').val(data);
    console.log(data);
});

jQuery('#main-search-form').submit(function() {
    jQuery('#main-search-form select').remove();
    return true;
});


// Hero

jQuery(document).ready(function () {
    
    var images = [
        'victimas-hero-random-small-1.jpg',
        'victimas-hero-random-small-2.jpg'
    ];
    jQuery('#main-hero').attr('style', 'background: url(' + SITE_URL + '/wp-content/uploads/2021/09/' + images[Math.floor(Math.random() * images.length)] + ') !important; background-position: center !important; background-repeat: no-repeat !important; background-size: cover !important;');

});