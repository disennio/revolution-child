<div id="content">
    <div id="results-container">
        
        <div class="title-container text-center">
            <h2>Resultados</h2>
        </div>

        <div class="results-controls">
            <input type="text" placeholder="Buscar">
            <a href="http://localhost:10044/wp-content/uploads/files/export.xlsx" class="export-results" >Exportar Datos</a>
        </div>

        <div id="results">
            <a href="" class="result">
                <div>
                    <span class="result-img-placeholder">
                        <img src="" alt="" class="">
                    </span>
                    <span><span>Nombre </span> <span>Apellido</span></span>
                </div>
                <div>
                    <span>Fecha desaparición</span>
                </div>
                <div>
                    <span>Obrero</span>
                </div>
                <div>
                    <span>Ejercito, Empresa</span>
                </div>
                <div>
                    <span>Magdalena</span>
                </div>
            </a>
            <a href="" class="result">
                <div>
                    <span class="result-img-placeholder">
                        <img src="" alt="" class="">
                    </span>
                    <span><span>Nombre </span> <span>Apellido</span></span>
                </div>
                <div>
                    <span>Fecha desaparición</span>
                </div>
                <div>
                    <span>Obrero</span>
                </div>
                <div>
                    <span>Ejercito, Empresa</span>
                </div>
                <div>
                    <span>Magdalena</span>
                </div>
            </a>
        </div>

    </div>
</div>
