<form id="main-search-form" action="/victimas" class="main-form-vidas">
    <div class="input-container">

        <div class="search-inputs">
        
            <div class="input-group one-third-col">
                <!--<span class="input-group">
                    <label for="">Nombre</label>
                    <span class="input" role="textbox" contenteditable></span>
                </span>
                <span class="input-group">
                    <label for="">Apellido</label>
                    <span class="input" role="textbox" contenteditable></span>
                </span>-->
                <input type="text" name="victim_name" placeholder="Nombre" class="text-md-left six-cols"  /> 
                <input type="text" name="victim_lastname" placeholder="Apellido" class="text-md-left six-cols"  />
            </div>
            <div class="input-group five-cols d-flex">
                <div class="seven-cols">
                    <input id="date1" type="text" placeholder="Fecha desde" class="seven-cols datepicker" autocomplete="off" name="victim_date" />
                    <input id="date2" type="text" placeholder="Fecha hasta" class="five-cols datepicker" autocomplete="off" name="victim_date_two" />
                </div>
                <?php 
                    // choose the field
                    $field_name = 'victim_sex';
                    $field = acf_get_field($field_name);
                    
                    // build the form
                    echo '<select name="' . $field['key'] . '" class="five-cols gender-select" data-placeholder="Género">';
                        echo '<option value=""></option>';
                        foreach( $field['choices'] as $k => $v )
                        {
                            echo '<option value="' . $k . '">' . $v . '</option>';
                        }
                    echo '</select>';
                    
                ?>
                <input class="gender-hidden" type="hidden" name="victim_gender">
            </div>
            <div class="input-group four-cols last-col">
                <?php 
                    // choose the field
                    $field_name = 'victim_event_type';
                    $field = acf_get_field($field_name);
                    
                    // build the form
                    echo '<select name="' . $field['key'] . '" id="" class="six-cols event-type-select" data-placeholder="Tipo de Victima">';
                        echo '<option value=""></option>';
                        foreach( $field['choices'] as $k => $v )
                        {
                            echo '<option value="' . $k . '">' . $v . '</option>';
                        }
                    echo '</select>';
                    
                ?>
                <input class="event-type-hidden" type="hidden" name="victim_event_type">
                <input class="perpetrators-hidden" type="hidden" name="victim_perpetrators">
                <?php 
                    // choose the field
                    $field_name = 'victim_perpetrators';
                    $field = acf_get_field($field_name);
                    
                    // build the form
                    echo '<select name="' . $field['key'] . '" id="" class="six-cols" data-placeholder="Responsable">';
                        echo '<option value=""></option>';
                        foreach( $field['choices'] as $k => $v )
                        {
                            echo '<option value="' . $k . '">' . $v . '</option>';
                        }
                    echo '</select>';
                    
                ?>
            </div>

        </div>

        <div class="search-inputs advance-search-inputs">
            <div class="input-group one-third-col">
                <select name="" id="" class="six-cols">
                    <option value="">Grupo Étnico</option>
                    <option value="1">Afrocolombiano</option>
                    <option value="2">Indígena</option>
                </select>
                <select name="" id="" class="six-cols">
                    <option value="">Étnia</option>
                    <option value="1">Arhuaco</option>
                    <option value="3">Arzario</option>
                    <option value="5">Awá</option>
                    <option value="6">Barí</option>
                    <option value="7">Cofán</option>
                    <option value="8">Coreguaje </option>
                    <option value="52">Coyaima</option>
                    <option value="9">Cubeo</option>
                    <option value="10">Cuiba</option>
                    <option value="11">Dojurá</option>
                    <option value="12">Embera</option>
                    <option value="13">Embera Chamí </option>
                    <option value="17">Embera Dóbida</option>
                    <option value="14">Embera Katío</option>
                    <option value="15">Embera Wounaan</option>
                    <option value="20">Guahibo</option>
                    <option value="22">Guambiano</option><option value="23">Hitnü</option><option value="24">Huitoto</option><option value="25">Indígena</option><option value="26">Inga</option><option value="28">Jugako</option><option value="29">Kankuamo</option><option value="30">Kogui</option><option value="31">Korewajú</option><option value="32">Kuna</option><option value="21">Macaguaje</option><option value="34">Nasa - Páez</option><option value="35">Orewa</option><option value="37">Panuré</option><option value="38">Pasto</option><option value="39">Pijao</option><option value="41">Sikuani</option><option value="0">Sin datos</option><option value="42">Totoró</option><option value="44">U´wa</option><option value="49">Waunaan</option><option value="47">Wayúu</option><option value="48">Wiwa</option><option value="50">Yanacona</option><option value="51">Zenú</option>
                </select>
            </div>
            <div class="input-group one-third-col">
                <select name="" id="" class="six-cols">
                    <option value="">Departamento</option>
                    <option></option><option value="1">Amazonas  </option><option value="2">Antioquia</option><option value="3">Arauca</option><option value="4">Atlántico </option><option value="5">Bolívar</option><option value="6">Bolívar/sucre</option><option value="7">Boyacá</option><option value="8">Brasil</option><option value="9">Caldas</option><option value="10">Caquetá</option><option value="11">Casanare</option><option value="12">Cauca</option><option value="13">Cauca/huila</option><option value="14">Cesar</option><option value="15">Chocó</option><option value="16">Córdoba</option><option value="17">Cundinamarca</option><option value="18">D.c.</option><option value="19">Ecuador</option><option value="20">Guaviare</option><option value="21">Huila</option><option value="22">La Guajira</option><option value="23">Magdalena</option><option value="24">Meta</option><option value="25">Meta (vichada)</option><option value="26">Nariño</option><option value="27">Norte de Santander</option><option value="28">Panama</option><option value="29">Putumayo</option><option value="30">Quindío</option><option value="31">Risaralda</option><option value="32">Risaralda / Chocó</option><option value="33">San Andrés</option><option value="34">Santander</option><option value="35">Sin información</option><option value="36">Sucre</option><option value="37">Tolima</option><option value="38">Tolima/Quindío/Caquetá</option><option value="39">Valle del Cauca</option><option value="40">Vaupés</option><option value="41">Venezuela</option><option value="42">Vichada</option>
                </select>
                <select name="" id="" class="six-cols">
                    <option value="">Ciudad</option>
                </select>
            </div>
            <div class="input-group one-third-col last-col">
                <input type="text" placeholder="Corregimiento" class="six-cols" />
                <input type="text" placeholder="Vereda" class="six-cols" />
            </div>
        </div>

    </div>

    <div class="controls-container">
        <button class="advance-search-button">
            <span>
                <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1"
                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <title>icon-filter</title>
                    <g id="Website" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Website-/-Victimas" transform="translate(-121.000000, -393.000000)"
                            stroke="#BBBBC1" stroke-width="2.38636364">
                            <g id="Group-8" transform="translate(122.000000, 392.500000)">
                                <g id="icon-filter" transform="translate(0.000000, 2.666667)">
                                    <line x1="0.257142857" y1="12.8043478" x2="9" y2="12.8043478" id="Line-Copy"
                                        stroke-linecap="round"></line>
                                    <line x1="9" y1="3.39707187" x2="17.7428571" y2="3.39707187" id="Line-Copy-3"
                                        stroke-linecap="round"></line>
                                    <line x1="15.6857143" y1="12.8043478" x2="17.7428571" y2="12.8043478"
                                        id="Line-Copy-2" stroke-linecap="round"></line>
                                    <line x1="0.257142857" y1="3.39707187" x2="2.31428571" y2="3.39707187"
                                        id="Line-Copy-4" stroke-linecap="round"></line>
                                    <path
                                        d="M12.3428571,9.40727595 C10.6386642,9.40727595 9.25714286,10.8112029 9.25714286,12.5430346 C9.25714286,14.2748663 10.6386642,15.6787933 12.3428571,15.6787933 C14.0470501,15.6787933 15.4285714,14.2748663 15.4285714,12.5430346 C15.4285714,10.8112029 14.0470501,9.40727595 12.3428571,9.40727595 L12.3428571,9.40727595 Z"
                                        id="Path_628-Copy"></path>
                                    <path
                                        d="M5.65714286,0 C3.95294991,0 2.57142857,1.40392697 2.57142857,3.13575865 C2.57142857,4.86759033 3.95294991,6.2715173 5.65714286,6.2715173 C7.3613358,6.2715173 8.74285714,4.86759033 8.74285714,3.13575865 C8.74285714,1.40392697 7.3613358,0 5.65714286,0 L5.65714286,0 Z"
                                        id="Path_628-Copy-2"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </span>
            <span class="advanced-search-closed">Búsqueda Avanzada</span>
            <span class="advanced-search-open">Búsqueda Simple</span>
        </button>
        <button class="form-main-button">
            <span>Buscar</span>
            <span>
                <svg width="14px" height="13px" viewBox="0 0 14 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Group" transform="translate(0.000000, 0.344361)" fill="#000000" fill-rule="nonzero">
                            <path d="M7.11262463,0 L13.3113103,6.15563922 L7.11262463,12.3112784 L5.81045229,11 L9.726,7.11 L0,7.110542 L0,5.262542 L9.789,5.262 L5.81045229,1.31127845 L7.11262463,0 Z" id="icon-arrow"></path>
                        </g>
                    </g>
                </svg>
            </span>
        </button>
    </div>

</form>