<?php get_header(); ?>

<?php
	$article_sidebar     = ot_get_option( 'article_sidebar', 'on' );
	$article_author_name = ot_get_option( 'article_author_name', 'on' );
	$article_date        = ot_get_option( 'article_date', 'on' );
	$article_cat         = ot_get_option( 'article_cat', 'on' );
?>
<article itemscope itemtype="http://schema.org/Article" <?php post_class( 'post post-detail style1-detail' ); ?>>
<?php
	$thb_id         = get_the_ID();
	$post_header_bg = get_post_meta( $thb_id, 'post_header_bg', true );
	$article_style  = ot_get_option( 'article_style', 'style1' );

	$article_sidebar     = ot_get_option( 'article_sidebar', 'on' );
	$article_author_name = ot_get_option( 'article_author_name', 'on' );
	$article_date        = ot_get_option( 'article_date', 'on' );
	$article_cat         = ot_get_option( 'article_cat', 'on' );
?>
<?php if ( 'style1' === $article_style ) { ?>
<figure class="post-gallery parallax post-gallery-detail">
	<div class="parallax_bg">
		<?php if ( $post_header_bg ) { ?>
			<style>
				.post-<?php echo esc_attr( $thb_id ); ?> .parallax_bg {
					<?php thb_bgoutput( $post_header_bg ); ?>
				}
			</style>
			<?php
		} else {
			the_post_thumbnail( 'revolution-wide-3x' ); }
		?>
	</div>

	<div class="header-spacer-force"></div>
	<header class="post-title entry-header animation bottom-to-top-3d">
		<div class="row align-center">
			<div class="small-12 medium-10 large-7 columns">
				<?php if ( 'on' === $article_cat ) { ?>
				<aside class="post-category">
					
				</aside>
				<?php } ?>
				<?php the_title( '<h1 class="entry-title" itemprop="name headline">', '</h1>' ); ?>
                
				<aside class="post-meta">
                    <?php 
                        $eventID = get_field('article_related_event'); 
                        //echo $eventID[0];
                        //var_dump( get_field('event_victims_count', $eventID[0]) );
                        $eventDate = get_field('event_date', $eventID[0]);
                        $eventVictims = get_field('event_victims_count', $eventID[0]);
                        $eventCity = get_field('event_location_city', $eventID[0]);
                        $eventDepartment = get_field('event_location_department', $eventID[0]);
                    ?>  
                    Fecha:
                    <?php 
                        echo $eventDate . "&nbsp; &nbsp;";
                    ?>
                    Número de Victimas: 
                    <?php 
                        echo $eventVictims . "&nbsp; &nbsp;";
                    ?>

                    Lugar: 
                    <?php 
                        echo $eventCity . ', ' . $eventDepartment;
                    ?>
                    
				</aside>
				
			</div>
		</div>
	</header>
</figure>
<?php } elseif ( has_post_thumbnail() ) { ?>
<figure class="post-gallery">
	<?php the_post_thumbnail( 'revolution-squaresmall-x3' ); ?>
</figure>
<?php } ?>

	<div class="row align-center mb-6">
		<div class="small-12 medium-10 large-7 columns">
			<div class="post-content">
                
				<?php the_content(); ?>
				
			</div>

            <?php include_once "sharing-options.php"; ?>
			
		</div>
		
        <aside class="sidebar" role="complementary">
	        <div class="sidebar_inner">
                <div class="event-map">
					<?php 
					$location = get_field('event_location_map');
					if( $location ): ?>
						<div class="acf-map" data-zoom="8">
							<div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
						</div>
					<?php endif; ?>
                </div>
                <div class="mt-3 events-container small">
                    
					<strong>Victimas:</strong><br><br>
					<?php 
						$eventID = get_field('article_related_event'); 
						//echo $eventID[0];
						//var_dump( get_field('event_victims', $eventID[0]) );
						$eventVictims = get_field('event_victims', $eventID[0]);
						echo '<ul class="victims-list">';
						foreach ( $eventVictims as $victim ) {
							echo  get_field('victim_name', $victim) . ' ' . get_field('victim_lastname', $victim); 
						}
						echo '</ul>';
					?>
                    
                </div>
			</div>
        </aside>
        
	</div>
	
	<?php do_action( 'thb_postmeta' ); ?>
</article>

<style type="text/css">
.acf-map {
	width: 100%;
	height: 300px;
	border: #ccc solid 1px;
	margin: 20px 0;
}
.acf-map img {
   max-width: inherit !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuw5YY-b3TCbuwgNpfa-HudIc4vS2cR0I"></script>
<script type="text/javascript">
(function( $ ) {

/**
 * initMap
 *
 * Renders a Google Map onto the selected jQuery element
 *
 * @date	22/10/19
 * @since	5.8.6
 *
 * @param	jQuery $el The jQuery element.
 * @return	object The map instance.
 */
function initMap( $el ) {
	
	// Find marker elements within map.
	var $markers = $el.find('.marker');
	
	// Create gerenic map.
	var mapArgs = {
		zoom		: $el.data('zoom') || 16,
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map( $el[0], mapArgs );
	
	// Add markers.
	map.markers = [];
	$markers.each(function(){
		initMarker( $(this), map );
	});
	
	// Center map based on markers.
	centerMap( map );
	
	// Return map instance.
	return map;
}

/**
 * initMarker
 *
 * Creates a marker for the given jQuery element and map.
 *
 * @date	22/10/19
 * @since	5.8.6
 *
 * @param	jQuery $el The jQuery element.
 * @param	object The map instance.
 * @return	object The marker instance.
 */
function initMarker( $marker, map ) {

	// Get position from marker.
	var lat = $marker.data('lat');
	var lng = $marker.data('lng');
	var latLng = {
		lat: parseFloat( lat ),
		lng: parseFloat( lng )
	};

	// Create marker instance.
	var marker = new google.maps.Marker({
		position : latLng,
		map: map
	});

	// Append to reference for later use.
	map.markers.push( marker );

	// If marker contains HTML, add it to an infoWindow.
	if( $marker.html() ){
		
		// Create info window.
		var infowindow = new google.maps.InfoWindow({
			content: $marker.html()
		});

		// Show info window when marker is clicked.
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open( map, marker );
		});
	}
}

/**
 * centerMap
 *
 * Centers the map showing all markers in view.
 *
 * @date	22/10/19
 * @since	5.8.6
 *
 * @param	object The map instance.
 * @return	void
 */
function centerMap( map ) {

	// Create map boundaries from all map markers.
	var bounds = new google.maps.LatLngBounds();
	map.markers.forEach(function( marker ){
		bounds.extend({
			lat: marker.position.lat(),
			lng: marker.position.lng()
		});
	});

	// Case: Single marker.
	if( map.markers.length == 1 ){
	    map.setCenter( bounds.getCenter() );
	
	// Case: Multiple markers.
	} else{
		map.fitBounds( bounds );
	}
}

// Render maps on page load.
$(document).ready(function(){
	$('.acf-map').each(function(){
		var map = initMap( $(this) );
	});
});

})(jQuery);
</script>

<?php
get_footer();
